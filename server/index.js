var express = require('express');
const bodyParser = require("body-parser");
var app = express();
var InitiateMongoServer = require('./config/db');
const user = require('./routes/user')

InitiateMongoServer();
app.use(bodyParser.json());

app.get('/', (req, res)=>{
    res.send({message: 'API is Working'})
});

app.use("/user", user);

app.listen('3001', (req, res)=>{
    console.log("listening to port : 3001")
});