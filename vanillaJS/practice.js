const prices = [10, 6, 8, 95, 101];

const sampleFunction = (a, b, ...rest) =>{
    console.log('a and b is:', a, b);
    rest.map((eachitem)=>{
        console.log(eachitem)
    })
    return a+b;
}

console.log('spread', ...prices);

const result = sampleFunction(...prices);